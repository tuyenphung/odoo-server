# sudo python3.7 -m pip install Pillow --upgrade (run this command to test install)
Pillow==9.1.0
markupsafe==2.0.1
lxml==4.8.0
decorator==5.1.1
polib==1.1.1
psycopg2-binary==2.9.3 # to prevent error psycopg2
psutil==5.4.2