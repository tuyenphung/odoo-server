from odoo import fields,models,api

class Partner(models.Model):
    _inherit = "res.partner"
    
    def create_crm(self, value):
        if value:
            self.env['crm.lead'].create([{'name':value['name'],'phone':value['phone'],'email_from':value['email'],'product':value['product'],'creater_contact':value['creater_contact'],'mkt_company':value['mkt_company'],'sale_team':value['sale_team']}])
        
    @api.model
    def create(self, value):
        if value:
            self.create_crm(value)
        return super(Partner, self).create(value)