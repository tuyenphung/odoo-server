# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.


{
    'name': 'Contacts crmcrm',
    
    'sequence': 1000,
    'summary': 'Centralize your address book',
    'description': """
This module gives you a quick view of your contacts directory, accessible from your home page.
You can track your vendors, customers and other contacts.
""",
    'depends': ['contacts', 'crm'],
    # 'data': [
    #     'views/contact_views.xml',
    # ],
    'application': True,
    'license': 'LGPL-3',
}
