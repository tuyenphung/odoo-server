odoo.define('res.partner.list', function (require) {
    "use strict";

    var core = require('web.core');
    var _t = core._t;
    var qweb = core.qweb;
    var ListController = require('web.ListController');
    var ListRenderer = require('web.ListRenderer');
    var ListView = require('web.ListView');
    var viewRegistry = require('web.view_registry');
    var ImportMenu = require('base_import.ImportMenu');

    ListController.include({
        events: _.extend({}, ListController.prototype.events, {
            'click .o_list_import_xlsx': 'import_records',
        }),

        import_records : function(e) {
            const { model, context } = this.model.get(this.handle, {raw: true});
            this.do_action({
                type: 'ir.actions.client',
                tag: 'import',
                params: {
                    model: model,
                    context: context,
                }
            });
        },

        is_import_enable: function() {
            var env = this.controlPanelProps;
            return env.view &&
                ['kanban', 'list'].includes(env.view.type) &&
                env.action.type === 'ir.actions.act_window' &&
                !!JSON.parse(env.view.arch.attrs.import || '1') &&
                !!JSON.parse(env.view.arch.attrs.create || '1');
        }
    });

    /*
        Distribute Contacts
    */

    var ResPartnerListController = ListController.extend({
        events: _.extend({}, ListController.prototype.events, {
            'click .button_distribute_contact': 'open_distribute_contact_wizard',
        }),

        /**
         * @private
         * @param {MouseEvent} event
         */
        open_distribute_contact_wizard: function (e) {
            var self = this;
            var ids = this.getSelectedIds();
            this._rpc({
                model: 'res.partner',
                method: 'action_distribute_contact',
                args: [ids],
            }).then(function (result) {
                self.do_action(result);
            });
        },

        _updateSelectionBox: function() {
            var { context } = this.model.get(this.handle, {raw: true});
            if ('distribute_contact_action' in context){
                if (this.$distributeContactButton) {
                    this.$distributeContactButton.remove();
                    this.$distributeContactButton = null;
                };
                if (this.selectedRecords.length) {
                    this.$distributeContactButton = $(qweb.render('DistributeContactButton', {}));
                    this.$distributeContactButton.appendTo(this.$buttons);
                };
            };
            this._super.apply(this, arguments);
        },
    });

    var ResPartnerListView = ListView.extend({
        config: _.extend({}, ListView.prototype.config, {
            Controller: ResPartnerListController,
        }),
    });

    viewRegistry.add('res_partner_list', ResPartnerListView);

    /*
        Distribute Quotations
    */

    var DistributeQuotationListController = ListController.extend({
        events: _.extend({}, ListController.prototype.events, {
            'click .button_distribute_quotation': 'open_distribute_quotation_wizard',
        }),

        /**
         * @private
         * @param {MouseEvent} event
         */
        open_distribute_quotation_wizard: function (e) {
            var self = this;
            var ids = this.getSelectedIds();
            this._rpc({
                model: 'sale.order',
                method: 'action_distribute_quotation',
                args: [ids],
            }).then(function (result) {
                self.do_action(result);
            });
        },

        _updateSelectionBox: function() {
            var { context } = this.model.get(this.handle, {raw: true});
            if ('distribute_quotation_action' in context){
                if (this.$distributeQuotationButton) {
                    this.$distributeQuotationButton.remove();
                    this.$distributeQuotationButton = null;
                };
                if (this.selectedRecords.length) {
                    this.$distributeQuotationButton = $(qweb.render('DistributeQuotationButton', {}));
                    this.$distributeQuotationButton.appendTo(this.$buttons);
                };
            };
            this._super.apply(this, arguments);
        },
    });

    var DistributeQuotationListView = ListView.extend({
        config: _.extend({}, ListView.prototype.config, {
            Controller: DistributeQuotationListController,
        }),
    });

    viewRegistry.add('distribute_quotation_list', DistributeQuotationListView);

    /*
        Contacts import
    */

    var ContactsImportListController = ListController.extend({
        buttons_template: 'ContactsImportListView.buttons',
        events: _.extend({}, ListController.prototype.events, {
            'click .btn-import-contacts': 'open_contacts_import_popup',
        }),

        /**
         * @private
         * @param {MouseEvent} event
         */
        open_contacts_import_popup: function (e) {
            var self = this;
            this._rpc({
                model: 'res.partner',
                method: 'open_contacts_import_popup',
                args: [""],
            }).then(function (result) {
                self.do_action(result);
            });
        },
    });

//    Add title for first button column
    var ContactsImportListRenderer = ListRenderer.extend({
        _renderHeaderCell: function (node) {
            var $th = this._super.apply(this, arguments);
            if (node.attrs.name == 'button_group_0') {
                $th.text('Duplicate')
            };
            return $th;
        },
    });

    var ContactsImportListView = ListView.extend({
        config: _.extend({}, ListView.prototype.config, {
            Controller: ContactsImportListController,
            Renderer: ContactsImportListRenderer,
        }),
    });

    viewRegistry.add('contacts_import_treeview', ContactsImportListView);

    /*
        Auto Distribute Contacts
    */

    var AutoDistributeContactListController = ListController.extend({
        buttons_template: 'AutoDistributeContactButtons',
        events: _.extend({}, ListController.prototype.events, {
            'click .by_ratio': 'open_auto_distribute_contact_by_ratio',
            'click .by_product': 'open_auto_distribute_contact_by_product',
        }),

        open_auto_distribute_contact_by_ratio: function (e) {
            if (e) {
                e.stopPropagation();
            };
            this.do_action({
                type: 'ir.actions.act_window',
                res_model: 'auto.distribute.contact.history',
                views: [[false, 'form']],
                res_id: undefined,
                context: {'default_type': 'ratio'},
            });
        },

        open_auto_distribute_contact_by_product: function (e) {
            if (e) {
                e.stopPropagation();
            };
            this.do_action({
                type: 'ir.actions.act_window',
                res_model: 'auto.distribute.contact.history',
                views: [[false, 'form']],
                res_id: undefined,
                context: {'default_type': 'product'},
            });
        },
    });

    var AutoDistributeContactListView = ListView.extend({
        config: _.extend({}, ListView.prototype.config, {
            Controller: AutoDistributeContactListController,
        }),
    });

    viewRegistry.add('auto_distribute_contact_tree_view', AutoDistributeContactListView);

    /*
        Search Contacts
    */

    var ContactReadonlyListRenderer = ListRenderer.extend({
        init: function (parent, state, params) {
            this._super.apply(this, arguments);
            this.hasSelectors = false;
        },
    });

    var ContactReadonlyListView = ListView.extend({
        config: _.extend({}, ListView.prototype.config, {
            Renderer: ContactReadonlyListRenderer,
        }),
    });

    viewRegistry.add('contact_readonly_tree_view', ContactReadonlyListView);

    /*
        Sale Order import
    */

    var SaleOrdersImportListController = ListController.extend({
        buttons_template: 'SaleOrderImportListView.buttons',
        events: _.extend({}, ListController.prototype.events, {
            'click .btn-import-orders': 'open_sale_order_import_popup',
        }),

        /**
         * @private
         * @param {MouseEvent} event
         */
        open_sale_order_import_popup: function (e) {
            var self = this;
            this._rpc({
                model: 'sale.order',
                method: 'open_sale_order_import_popup',
                args: [""],
            }).then(function (result) {
                self.do_action(result);
            });
        },
    });


    var SaleOrderImportListView = ListView.extend({
        config: _.extend({}, ListView.prototype.config, {
            Controller: SaleOrdersImportListController
        }),
    });

    viewRegistry.add('sale_order_view_order_tree', SaleOrderImportListView);

    /*
        Hide create button in sale order tree view
    */

    var QuotationListController = ListController.extend({
        buttons_template: 'QuotationListView.buttons',
    });


    var QuotationListView = ListView.extend({
        config: _.extend({}, ListView.prototype.config, {
            Controller: QuotationListController
        }),
    });

    viewRegistry.add('sale_order_view_quotation_tree', QuotationListView);
});