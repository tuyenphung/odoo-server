# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

{
    'name': 'Sales and stock sttstt',
    'version': '1.0',
    'category': 'Hidden',
    'summary': 'Quotation, Sales Orders, Delivery & Invoicing Control',
    'description': """
Manage sales quotations and orders
==================================

This module makes the link between the sales and warehouses management applications.

Preferences
-----------
* Shipping: Choice of delivery at once or partial delivery
* Invoicing: choose how invoices will be paid
* Incoterms: International Commercial terms

""",
    'depends': ['sale','stock'],
    'data': [
        "views/sale_order_views.xml",
        
    ],
 
    'assets': {
        'web.assets_backend': [
        'sale_stock_stt/static/src/list_controller.js',
        ],
        'web.assets_qweb': [
            'sale_stock_stt/static/src/contacts_import_templates.xml',
        ],
    },
    'installable': True,
    'auto_install': True,
    
    'license': 'LGPL-3',
}
