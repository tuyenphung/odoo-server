from odoo import fields, models, api


class SaleOrder(models.Model):
    _inherit = "sale.order"

    delivery_status = fields.Char(string="Delivery Status")
    so_status = fields.Selection(string="SO Status", selection=[('quotation', 'Quatation'), ('Confirmed', 'confirmed'), ('delivering', 'Delivering'), ('done', 'Done')],compute='_compute_so_status')
 
    @api.depends('picking_ids')
    def _compute_so_status(self):
        for r in self:
            if r.picking_ids:
                pick=self.env['stock.picking']
                out=self.env['stock.picking']
                for pic in r.picking_ids:
                    if pic.picking_type_id.id == 3:
                        pick=pic
                    elif pic.picking_type_id.id ==2:
                        out=pic
                if pick.state == "done" and out.state == "done":
                    r.so_status = "done"
                elif pick.state == "done" and out.state != "done":
                    r.so_status = "delivering"
                elif pick and out:
                    r.so_status = "Confirmed"
                else:
                    r.so_status = False
            else:
                r.so_status=False  


    def write(self,list_vals):
        res = super(SaleOrder, self).write(list_vals)
        if "delivery_status" in list_vals:
            delivery_status =list_vals["delivery_status"]
            if delivery_status =="Order Created":
                pass
            if delivery_status =="For Pick Up":
                self.picking_ids.filtered_domain([('picking_type_id.id','=',2)])[0].state="waiting"
                self.picking_ids.filtered_domain([('picking_type_id.id','=',3)])[0].state="confirmed"
            if delivery_status =="Picked Up":
                self.picking_ids.filtered_domain([('picking_type_id.id','=',2)])[0].state="assigned"
                self.picking_ids.filtered_domain([('picking_type_id.id','=',3)])[0].state="done"
            if delivery_status =="Delivered":
                self.picking_ids.filtered_domain([('picking_type_id.id','=',2)])[0].state="done"
                self.picking_ids.filtered_domain([('picking_type_id.id','=',3)])[0].state="done"
        else:
                return res
        return res

    @api.model
    def create(self,list_vals):
        res = super(SaleOrder, self).create(list_vals)
        for r in res:
            if r.delivery_status:
                delivery_status =r.delivery_status
                if delivery_status =="Order Created":
                    pass
                if delivery_status =="For Pick Up" and r.state != "sale":
                    r.action_confirm()
                    r.picking_ids.filtered_domain([('picking_type_id.id','=',2)])[0].state="waiting"
                    r.picking_ids.filtered_domain([('picking_type_id.id','=',3)])[0].state="confirmed" 
                if delivery_status =="Picked Up" and r.state =="sale":
                    r.picking_ids.filtered_domain([('picking_type_id.id','=',2)])[0].state="assigned"
                    r.picking_ids.filtered_domain([('picking_type_id.id','=',3)])[0].state="done"
                if delivery_status =="Picked Up" and r.state !="sale":
                    r.action_confirm()
                    r.picking_ids.filtered_domain([('picking_type_id.id','=',2)])[0].state="assigned"
                    r.picking_ids.filtered_domain([('picking_type_id.id','=',3)])[0].state="done"
                if delivery_status =="Delivered" and r.state =="sale":
                    r.picking_ids.filtered_domain([('picking_type_id.id','=',2)])[0].state="done"
                    r.picking_ids.filtered_domain([('picking_type_id.id','=',3)])[0].state="done"
                if delivery_status =="Delivered" and r.state !="sale":
                    r.action_confirm()
                    r.picking_ids.filtered_domain([('picking_type_id.id','=',2)])[0].state="done"
                    r.picking_ids.filtered_domain([('picking_type_id.id','=',3)])[0].state="done"
            else:
                return res
            return res

    def action_update_status(self):
        pass
