# Translation of Odoo Server.
# This file contains the translation of the following modules:
# 	* hr_expense_extract
# 
# Translators:
# Vo Thanh Thuy, 2021
# Martin Trigaux, 2021
# fanha99 <fanha99@hotmail.com>, 2021
# thanhnguyen.icsc <thanhnguyen.icsc@gmail.com>, 2021
# Duy BQ <duybq86@gmail.com>, 2021
# Trần Hà <tranthuha13590@gmail.com>, 2021
# 
msgid ""
msgstr ""
"Project-Id-Version: Odoo Server saas~14.5+e\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-09-14 11:00+0000\n"
"PO-Revision-Date: 2021-09-14 12:40+0000\n"
"Last-Translator: Trần Hà <tranthuha13590@gmail.com>, 2021\n"
"Language-Team: Vietnamese (https://www.transifex.com/odoo/teams/41243/vi/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: \n"
"Language: vi\n"
"Plural-Forms: nplurals=1; plural=0;\n"

#. module: hr_expense_extract
#: model:ir.model.fields,field_description:hr_expense_extract.field_expense_sample_register__amount
msgid "Amount"
msgstr "Tổng tiền"

#. module: hr_expense_extract
#: code:addons/hr_expense_extract/models/hr_expense.py:0
#: model:ir.model.fields.selection,name:hr_expense_extract.selection__hr_expense__extract_state__error_status
#, python-format
msgid "An error occurred"
msgstr "Một lỗi đã xảy ra"

#. module: hr_expense_extract
#: model:ir.model,name:hr_expense_extract.model_ir_attachment
msgid "Attachment"
msgstr "Đính kèm"

#. module: hr_expense_extract
#: model:ir.model.fields,field_description:hr_expense_extract.field_expense_sample_register__available_payment_method_line_ids
msgid "Available Payment Method Line"
msgstr ""

#. module: hr_expense_extract
#: model_terms:ir.ui.view,arch_db:hr_expense_extract.hr_expense_extract_view_form
msgid "Buy credits"
msgstr "Buy credits"

#. module: hr_expense_extract
#: model:ir.model.fields,field_description:hr_expense_extract.field_hr_expense__extract_can_show_resend_button
msgid "Can show the ocr resend button"
msgstr "Can show the ocr resend button"

#. module: hr_expense_extract
#: model:ir.model.fields,field_description:hr_expense_extract.field_hr_expense__extract_can_show_send_button
msgid "Can show the ocr send button"
msgstr "Can show the ocr send button"

#. module: hr_expense_extract
#: model_terms:ir.ui.view,arch_db:hr_expense_extract.expense_sample_register_view_form
msgid "Cancel"
msgstr "Hủy"

#. module: hr_expense_extract
#. openerp-web
#: code:addons/hr_expense_extract/static/src/js/tours/expense_tour.js:0
#, python-format
msgid "Choose a receipt."
msgstr ""

#. module: hr_expense_extract
#: model_terms:ir.ui.view,arch_db:hr_expense_extract.expense_sample_receipt_view_form
msgid "Choose a receipt:"
msgstr ""

#. module: hr_expense_extract
#: model:ir.model,name:hr_expense_extract.model_res_company
msgid "Companies"
msgstr "Công ty"

#. module: hr_expense_extract
#: model:ir.model.fields,field_description:hr_expense_extract.field_expense_sample_register__company_id
msgid "Company"
msgstr "Công ty"

#. module: hr_expense_extract
#: model:ir.model.fields.selection,name:hr_expense_extract.selection__hr_expense__extract_state__done
msgid "Completed flow"
msgstr "Completed flow"

#. module: hr_expense_extract
#: model:ir.model,name:hr_expense_extract.model_res_config_settings
msgid "Config Settings"
msgstr "Thiết lập cấu hình"

#. module: hr_expense_extract
#: model_terms:ir.ui.view,arch_db:hr_expense_extract.expense_sample_register_view_form
msgid "Create Payment"
msgstr "Tạo thanh toán"

#. module: hr_expense_extract
#: model:ir.model.fields,field_description:hr_expense_extract.field_expense_sample_receipt__create_uid
#: model:ir.model.fields,field_description:hr_expense_extract.field_expense_sample_register__create_uid
#: model:ir.model.fields,field_description:hr_expense_extract.field_hr_expense_extract_words__create_uid
msgid "Created by"
msgstr "Tạo bởi"

#. module: hr_expense_extract
#: model:ir.model.fields,field_description:hr_expense_extract.field_expense_sample_receipt__create_date
#: model:ir.model.fields,field_description:hr_expense_extract.field_expense_sample_register__create_date
#: model:ir.model.fields,field_description:hr_expense_extract.field_hr_expense_extract_words__create_date
msgid "Created on"
msgstr "Thời điểm tạo"

#. module: hr_expense_extract
#: model:ir.model.fields,field_description:hr_expense_extract.field_expense_sample_register__currency_id
msgid "Currency"
msgstr "Tiền tệ"

#. module: hr_expense_extract
#: model:ir.model.fields.selection,name:hr_expense_extract.selection__res_company__expense_extract_show_ocr_option_selection__auto_send
msgid "Digitalize automatically"
msgstr ""

#. module: hr_expense_extract
#: model:ir.model.fields.selection,name:hr_expense_extract.selection__res_company__expense_extract_show_ocr_option_selection__manual_send
msgid "Digitalize on demand only"
msgstr ""

#. module: hr_expense_extract
#: model:ir.model.fields,field_description:hr_expense_extract.field_expense_sample_receipt__display_name
#: model:ir.model.fields,field_description:hr_expense_extract.field_expense_sample_register__display_name
#: model:ir.model.fields,field_description:hr_expense_extract.field_hr_expense_extract_words__display_name
msgid "Display Name"
msgstr "Tên hiển thị"

#. module: hr_expense_extract
#: model:ir.model.fields.selection,name:hr_expense_extract.selection__res_company__expense_extract_show_ocr_option_selection__no_send
msgid "Do not digitalize"
msgstr ""

#. module: hr_expense_extract
#: model:ir.model.fields,field_description:hr_expense_extract.field_hr_expense__extract_error_message
msgid "Error message"
msgstr "Thông báo lỗi"

#. module: hr_expense_extract
#: model:ir.model,name:hr_expense_extract.model_hr_expense
#: model:ir.model.fields,field_description:hr_expense_extract.field_expense_sample_register__sheet_id
#: model:ir.model.fields,field_description:hr_expense_extract.field_hr_expense_extract_words__expense_id
msgid "Expense"
msgstr "Chi phí"

#. module: hr_expense_extract
#: model:ir.actions.server,name:hr_expense_extract.ir_cron_update_ocr_status_ir_actions_server
#: model:ir.cron,cron_name:hr_expense_extract.ir_cron_update_ocr_status
#: model:ir.cron,name:hr_expense_extract.ir_cron_update_ocr_status
msgid "Expense OCR: Update All Status"
msgstr ""

#. module: hr_expense_extract
#: model:ir.model,name:hr_expense_extract.model_hr_expense_sheet
msgid "Expense Report"
msgstr "Kê khai chi tiêu"

#. module: hr_expense_extract
#: model:ir.model.fields,help:hr_expense_extract.field_hr_expense__extract_remote_id
msgid "Expense extract id"
msgstr ""

#. module: hr_expense_extract
#: model:ir.model.fields,field_description:hr_expense_extract.field_res_config_settings__expense_extract_show_ocr_option_selection
msgid "Expense processing option"
msgstr ""

#. module: hr_expense_extract
#: code:addons/hr_expense_extract/models/hr_expense.py:0
#, python-format
msgid "Expenses sent"
msgstr ""

#. module: hr_expense_extract
#: model:ir.model.fields,field_description:hr_expense_extract.field_hr_expense__extract_word_ids
msgid "Extract Word"
msgstr "Extract Word"

#. module: hr_expense_extract
#: model:ir.model.fields,field_description:hr_expense_extract.field_hr_expense__extract_state
msgid "Extract state"
msgstr "Extract state"

#. module: hr_expense_extract
#: model:ir.model,name:hr_expense_extract.model_hr_expense_extract_words
msgid "Extracted words from expense scan"
msgstr ""

#. module: hr_expense_extract
#: code:addons/hr_expense_extract/models/hr_expense.py:0
#, python-format
msgid "Generated Expense"
msgstr "Generated Expense"

#. module: hr_expense_extract
#: model:ir.model.fields,field_description:hr_expense_extract.field_expense_sample_register__hide_partial
msgid "Hide Partial"
msgstr ""

#. module: hr_expense_extract
#: model:ir.model.fields,field_description:hr_expense_extract.field_expense_sample_register__hide_payment_method_line
msgid "Hide Payment Method Line"
msgstr ""

#. module: hr_expense_extract
#: model:ir.model.fields,field_description:hr_expense_extract.field_expense_sample_receipt__id
#: model:ir.model.fields,field_description:hr_expense_extract.field_expense_sample_register__id
#: model:ir.model.fields,field_description:hr_expense_extract.field_hr_expense_extract_words__id
msgid "ID"
msgstr "ID"

#. module: hr_expense_extract
#: model:ir.model.fields,field_description:hr_expense_extract.field_hr_expense__extract_remote_id
msgid "Id of the request to IAP-OCR"
msgstr "Id of the request to IAP-OCR"

#. module: hr_expense_extract
#: model:ir.model.fields,field_description:hr_expense_extract.field_expense_sample_register__journal_id
msgid "Journal"
msgstr "Sổ nhật ký"

#. module: hr_expense_extract
#: model:ir.model.fields.selection,name:hr_expense_extract.selection__expense_sample_register__partial_mode__open
msgid "Keep open"
msgstr "Giữ vẫn mở"

#. module: hr_expense_extract
#: model:ir.model.fields,field_description:hr_expense_extract.field_expense_sample_receipt____last_update
#: model:ir.model.fields,field_description:hr_expense_extract.field_expense_sample_register____last_update
#: model:ir.model.fields,field_description:hr_expense_extract.field_hr_expense_extract_words____last_update
msgid "Last Modified on"
msgstr "Sửa lần cuối vào"

#. module: hr_expense_extract
#: model:ir.model.fields,field_description:hr_expense_extract.field_expense_sample_receipt__write_uid
#: model:ir.model.fields,field_description:hr_expense_extract.field_expense_sample_register__write_uid
#: model:ir.model.fields,field_description:hr_expense_extract.field_hr_expense_extract_words__write_uid
msgid "Last Updated by"
msgstr "Cập nhật lần cuối bởi"

#. module: hr_expense_extract
#: model:ir.model.fields,field_description:hr_expense_extract.field_expense_sample_receipt__write_date
#: model:ir.model.fields,field_description:hr_expense_extract.field_expense_sample_register__write_date
#: model:ir.model.fields,field_description:hr_expense_extract.field_hr_expense_extract_words__write_date
msgid "Last Updated on"
msgstr "Cập nhật lần cuối vào"

#. module: hr_expense_extract
#: model_terms:ir.ui.view,arch_db:hr_expense_extract.expense_sample_receipt_view_form
msgid ""
"Let's try a sample receipt to test the automated processing of expenses with"
" Artificial Intelligence."
msgstr ""

#. module: hr_expense_extract
#: model:ir.model.fields,help:hr_expense_extract.field_expense_sample_register__payment_method_line_id
msgid ""
"Manual: Pay or Get paid by any method outside of Odoo.\n"
"Payment Acquirers: Each payment acquirer has its own Payment Method. Request a transaction on/to a card thanks to a payment token saved by the partner when buying or subscribing online.\n"
"Check: Pay bills by check and print it from Odoo.\n"
"Batch Deposit: Collect several customer checks at once generating and submitting a batch deposit to your bank. Module account_batch_payment is necessary.\n"
"SEPA Credit Transfer: Pay in the SEPA zone by submitting a SEPA Credit Transfer file to your bank. Module account_sepa is necessary.\n"
"SEPA Direct Debit: Get paid in the SEPA zone thanks to a mandate your partner will have granted to you. Module account_sepa is necessary.\n"
msgstr ""

#. module: hr_expense_extract
#: model:ir.model.fields.selection,name:hr_expense_extract.selection__expense_sample_register__partial_mode__paid
msgid "Mark as fully paid"
msgstr "Đánh dấu đã trả đủ"

#. module: hr_expense_extract
#: model:ir.model.fields,field_description:hr_expense_extract.field_expense_sample_register__memo
msgid "Memo"
msgstr "Nội dung giao dịch"

#. module: hr_expense_extract
#: code:addons/hr_expense_extract/models/hr_expense.py:0
#, python-format
msgid "No document name provided"
msgstr "No document name provided"

#. module: hr_expense_extract
#: model:ir.model.fields.selection,name:hr_expense_extract.selection__hr_expense__extract_state__no_extract_requested
msgid "No extract requested"
msgstr "No extract requested"

#. module: hr_expense_extract
#: model:ir.model.fields.selection,name:hr_expense_extract.selection__hr_expense__extract_state__not_enough_credit
msgid "Not enough credit"
msgstr "Not enough credit"

#. module: hr_expense_extract
#: model:ir.model.fields,field_description:hr_expense_extract.field_expense_sample_register__date
msgid "Payment Date"
msgstr "Ngày thanh toán"

#. module: hr_expense_extract
#: model:ir.model.fields,field_description:hr_expense_extract.field_expense_sample_register__partial_mode
msgid "Payment Difference"
msgstr "Thanh toán chênh lệch"

#. module: hr_expense_extract
#: model:ir.model.fields,field_description:hr_expense_extract.field_expense_sample_register__payment_method_line_id
msgid "Payment Method"
msgstr "Phương thức thanh toán"

#. module: hr_expense_extract
#: model_terms:ir.ui.view,arch_db:hr_expense_extract.expense_sample_register_view_form
msgid "Register Payment"
msgstr "Ghi nhận thanh toán"

#. module: hr_expense_extract
#: model:ir.actions.act_window,name:hr_expense_extract.action_expense_sample_register
msgid "Register Sample Payment"
msgstr ""

#. module: hr_expense_extract
#: model:ir.model,name:hr_expense_extract.model_expense_sample_register
msgid "Register Sample Payments"
msgstr ""

#. module: hr_expense_extract
#. openerp-web
#: code:addons/hr_expense_extract/static/src/js/tours/expense_tour.js:0
#, python-format
msgid "Report this expense to your manager for validation."
msgstr ""

#. module: hr_expense_extract
#: model_terms:ir.ui.view,arch_db:hr_expense_extract.hr_expense_extract_view_form
msgid "Resend For Digitalization"
msgstr "Resend For Digitalization"

#. module: hr_expense_extract
#: code:addons/hr_expense_extract/wizard/expense_sample_receipt.py:0
#, python-format
msgid "Sample Employee"
msgstr ""

#. module: hr_expense_extract
#: code:addons/hr_expense_extract/wizard/expense_sample_receipt.py:0
#, python-format
msgid "Sample Receipt: %s"
msgstr ""

#. module: hr_expense_extract
#: model_terms:ir.ui.view,arch_db:hr_expense_extract.hr_expense_extract_view_form
msgid "Send For Digitalization"
msgstr "Send For Digitalization"

#. module: hr_expense_extract
#: model:ir.actions.server,name:hr_expense_extract.hr_expense_parse_action_server
msgid "Send for digitalization"
msgstr ""

#. module: hr_expense_extract
#: model:ir.model.fields,field_description:hr_expense_extract.field_res_company__expense_extract_show_ocr_option_selection
msgid "Send mode on expense attachments"
msgstr ""

#. module: hr_expense_extract
#: code:addons/hr_expense_extract/models/hr_expense.py:0
#, python-format
msgid "Server is currently under maintenance. Please retry later"
msgstr "Server is currently under maintenance. Please retry later"

#. module: hr_expense_extract
#: code:addons/hr_expense_extract/models/hr_expense.py:0
#, python-format
msgid "Server not available. Please retry later"
msgstr "Server not available. Please retry later"

#. module: hr_expense_extract
#: model:ir.model.fields,field_description:hr_expense_extract.field_hr_expense__extract_status_code
msgid "Status code"
msgstr "Status code"

#. module: hr_expense_extract
#: model_terms:ir.ui.view,arch_db:hr_expense_extract.hr_expense_extract_view_form
msgid ""
"The data extraction is not finished yet. The extraction usually takes "
"between 5 and 60 seconds."
msgstr ""
"The data extraction is not finished yet. The extraction usually takes "
"between 5 and 60 seconds."

#. module: hr_expense_extract
#: code:addons/hr_expense_extract/models/hr_expense.py:0
#, python-format
msgid "The document could not be found"
msgstr "The document could not be found"

#. module: hr_expense_extract
#: model_terms:ir.ui.view,arch_db:hr_expense_extract.hr_expense_extract_view_form
msgid ""
"The file has been sent and is being processed. It usually takes between 5 "
"and 60 seconds."
msgstr ""

#. module: hr_expense_extract
#: model:ir.actions.act_window,name:hr_expense_extract.action_expense_sample_receipt
#: model_terms:ir.ui.view,arch_db:hr_expense_extract.expense_sample_receipt_view_form
msgid "Try Sample Receipt"
msgstr ""

#. module: hr_expense_extract
#: model:ir.model,name:hr_expense_extract.model_expense_sample_receipt
msgid "Try Sample Receipts"
msgstr ""

#. module: hr_expense_extract
#. openerp-web
#: code:addons/hr_expense_extract/static/src/js/tours/expense_tour.js:0
#, python-format
msgid "Try the AI with a sample receipt."
msgstr ""

#. module: hr_expense_extract
#: code:addons/hr_expense_extract/models/hr_expense.py:0
#, python-format
msgid "Unsupported image format"
msgstr "Unsupported image format"

#. module: hr_expense_extract
#: model_terms:ir.ui.view,arch_db:hr_expense_extract.hr_expense_extract_view_form
msgid "Update status"
msgstr ""

#. module: hr_expense_extract
#: model_terms:ir.ui.view,arch_db:hr_expense_extract.res_config_settings_view_form
msgid "View My Services"
msgstr "View My Services"

#. module: hr_expense_extract
#: model:ir.model.fields.selection,name:hr_expense_extract.selection__hr_expense__extract_state__waiting_extraction
msgid "Waiting extraction"
msgstr "Waiting extraction"

#. module: hr_expense_extract
#: model:ir.model.fields.selection,name:hr_expense_extract.selection__hr_expense__extract_state__waiting_validation
msgid "Waiting validation"
msgstr "Waiting validation"

#. module: hr_expense_extract
#. openerp-web
#: code:addons/hr_expense_extract/static/src/js/tours/expense_tour.js:0
#, python-format
msgid "Wasting time recording your receipts? Let’s try a better way."
msgstr ""

#. module: hr_expense_extract
#: model:ir.model.fields,field_description:hr_expense_extract.field_hr_expense_extract_words__word_page
msgid "Word Page"
msgstr "Word Page"

#. module: hr_expense_extract
#: model:ir.model.fields,field_description:hr_expense_extract.field_hr_expense_extract_words__word_text
msgid "Word Text"
msgstr "Word Text"

#. module: hr_expense_extract
#: code:addons/hr_expense_extract/models/hr_expense.py:0
#, python-format
msgid "You cannot send a expense that is not in draft state!"
msgstr ""

#. module: hr_expense_extract
#: model_terms:ir.ui.view,arch_db:hr_expense_extract.hr_expense_extract_view_form
msgid "You don't have enough credit to extract data from your expense."
msgstr ""

#. module: hr_expense_extract
#: code:addons/hr_expense_extract/models/hr_expense.py:0
#, python-format
msgid "You must send the same quantity of documents and file names"
msgstr "You must send the same quantity of documents and file names"

#. module: hr_expense_extract
#. openerp-web
#: code:addons/hr_expense_extract/static/src/js/tours/expense_tour.js:0
#: code:addons/hr_expense_extract/static/src/js/tours/expense_tour.js:0
#, python-format
msgid "Your manager will have to approve (or refuse) your expense reports."
msgstr ""

#. module: hr_expense_extract
#: model:ir.model.fields,help:hr_expense_extract.field_hr_expense_extract_words__expense_id
msgid "expense id"
msgstr ""

#. module: hr_expense_extract
#: model:ir.model.fields.selection,name:hr_expense_extract.selection__hr_expense__extract_state__extract_not_ready
msgid "waiting extraction, but it is not ready"
msgstr "waiting extraction, but it is not ready"
