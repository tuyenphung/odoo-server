FROM ubuntu:20.04
MAINTAINER TUYEN <tuyen@pv>

# Generate locale C.UTF-8 for postgres and general locale data
ENV LANG C.UTF-8

ARG DEBIAN_FRONTEND=noninteractive

# Generate locales
RUN apt-get update \
  && apt-get install -y apt-utils \
  && apt-get -yq install locales \
  && locale-gen en_US.UTF-8 \
  && update-locale LC_ALL=en_US.UTF-8 LANG=en_US.UTF-8

# Add user
RUN adduser --system --quiet --shell=/bin/bash --home=/opt/odoo/ --gecos 'odoo' --group odoo

# Create Log dir
RUN mkdir /var/log/odoo && \
    chown -R odoo:odoo /var/log/odoo

# tạo folder chứa file config
RUN mkdir /etc/odoo && \
    chown -R odoo:odoo /etc/odoo

# Install postgresql client, it help to fix error Database restore error: Command `psql` not found. (and pg_dump too)
RUN apt-get update && \
    apt install postgresql && \
    sudo service postgresql start && \
    sudo -u postgres psql && \
    CREATE USER odoo WITH LOGIN SUPERUSER CREATEDB CREATEROLE INHERIT REPLICATION CONNECTION LIMIT -1; && \
    ALTER USER postgres PASSWORD 'odoo';

# Install some deps, lessc and less-plugin-clean-css
# THANH - install libpq-dev to prevent error pg_config executable not found. xảy ra khi cài psycopg2==2.8.6 trong file requirements.txt
RUN apt-get update && \
    DEBIAN_FRONTEND=noninteractive apt-get install -y tzdata && \
    apt-get install software-properties-common -y && \
    add-apt-repository --yes ppa:deadsnakes/ppa  && \
    apt-get update && \
    apt-get install -y --no-install-recommends \
        ca-certificates \
        curl \
        dirmngr \
        git \
        openssh-client \
        python3.7-dev \
        python3.7-venv \
   build-essential \
  libldap2-dev \
  libsasl2-dev \
  libpq-dev

# Aeroo Report
RUN apt-get install -f libreoffice libreoffice-common libreoffice-base libreoffice-java-common -y && \
    apt-get install -y python3-pypdf2

# Install wkhtmltopdf based on QT5
ADD https://github.com/wkhtmltopdf/wkhtmltopdf/releases/download/0.12.5/wkhtmltox_0.12.5-1.focal_amd64.deb \
  /opt/odoo/wkhtmltox.deb

RUN apt-get update \
  && apt-get install -yq xfonts-base xfonts-75dpi \
  && dpkg -i /opt/odoo/wkhtmltox.deb

# Get Odoo15 Enterprise
RUN mkdir /opt/odoo/odoo-server && \
    cd /opt/odoo/odoo-server
RUN chown -R odoo:odoo /opt/odoo/odoo-server
WORKDIR /opt/odoo/odoo-server
# Copy all file Odoo 15e sources từ host vào trong image /opt/odoo/odoo-server
COPY . .
# Remove .git folder in order to reduce image size
RUN rm -rf .git

# Install python lib for Odoo
WORKDIR /opt/odoo

# Create python 3.7 virtual environment odoo
RUN python3.7 -m venv odoo-venv
RUN /opt/odoo/odoo-venv/bin/python3.7 -m pip install --upgrade pip

#Install python lib from requirements.txt of Odoo15 Enterprise
RUN /opt/odoo/odoo-venv/bin/pip install wheel \
    && /opt/odoo/odoo-venv/bin/pip install -r odoo-server/requirements.txt

#Install odoo and click-odoo-contrib (support CICD)
RUN /opt/odoo/odoo-venv/bin/pip install odoo-server/. && \
    /opt/odoo/odoo-venv/bin/pip install Pygments click-odoo-contrib

#Install Cerberus (sử dụng ở module eton_api, dùng cho các dự án sài Running Log như Eton)
RUN /opt/odoo/odoo-venv/bin/pip install cerberus

WORKDIR /opt/odoo
EXPOSE 8069 
ADD odoo.conf /etc/odoo/odoo.conf
RUN chown odoo /etc/odoo/odoo.conf

#Stream logs to stdout
RUN ln -sf /proc/self/fd/1 /var/log/odoo/odoo-server.log
ENTRYPOINT ["/opt/odoo/odoo-venv/bin/python3", "/opt/odoo/odoo-server/odoo-bin", "-c" , "/etc/odoo/odoo.conf"]
